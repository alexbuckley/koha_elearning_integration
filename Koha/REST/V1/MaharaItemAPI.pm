package Koha::REST::V1::MaharaItemAPI;

# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

use Modern::Perl;

use Mojo::Base 'Mojolicious::Controller';

use C4::Items;
use C4::Koha;
use C4::Biblio;
use Koha::Review;
use Koha::Reviews;
use Koha::Patrons;
use Data::Dumper;
#use WWW::Curl;
use Zend/Http/Client;

use Try::Tiny;

#my $curl = WWW::curl::Easy->new; 

sub authenticate_to_mahara {

    my $serverurl = 'https://mahara-integration.catalystdemo.net.nz/?alt=json'

    my $params = array(array( 'username': $username, 'password' => $password);

    my $client = new Zend_Http_Client($serverurl);
    try {
        $client->setParameterPost($params);
        my $token = $client->request('POST');
        Dumper($token);
        return $token;
    } catch {
        Dumper($token);
    }
}

sub get_student_blog_post {
    my ($student_id, $token) = @_;

    #Set the web service url server
    my $serverurl = 'https://mahara-integration.catalystdemo.net.nz/webservice/rest/server.php?alt=json';

    my $params = array(array( 'users_0_username': $student_id, 'wstoken' => $token), 'wsfunction' => 'mahara_blog_get_blogs_for_user');

    my $client = HTTP::Request($serverurl);
    try {
        $client->setParameterPost($params);
        my $response = $client->request('GET');
        Dumper($response->getBody());
    } catch ( exception $exception) {P
        Dumper($exception);
    }
}

sub create_student_blog_post {
    my ($student_id, $blog_id, $title, $description) = @_;

    #Set the web service url server
    my $serverurl = 'https://mahara-integration.catalystdemo.net.nz/webservice/rest/server.phpi?alt=json';

    #Set the parameters to call the webservice API with
    my $params = array(array( 'id' => 5028, 'title' => 'test 2 blog'), 'inputs' => array(0=>array('owner'=>$student_id, 'blogid'=>$blog_id, 'title'=>$title, 'description'=>$description)),
    'wsfunction' => 'mahara_blog_get_blogs_for_user', #The function to be called
    'wstoken' => $token,
    );

    my $client = new Zend_Http_client($serverurl);
    try {
        $client->setParameterPost($params);
        my $response = $client->request('POST');
        Dumper($response->getBody());
    } catch ( exception $exception) {
        Dumper($exception);
    }
}

#    $curl->setopt(CURLOPT_HEADER,1);
#   $curl->setopt(CURLOPT_URL, 'https://mahara-integration.catalystdemo.net.nz/webservice/rest/server.php?wstoken=223cd9046058a5b0a06d8f263bcdcf97&wsfunction=mahara_blog_get_blogs_for_user&alt=json');

#   my $blog_id = $curl->setopt($curl, CURLOPT_POST, $student_ID); #Note this curl code is for php and it needs to be changed to Perl. The same situation below. 

#   my $response;
#   $curl->setopt(CURLOPT_WRITEDATA, \$response);

#   my $req_code = $curl->perform;

#   if ($response == 0) {
#       print("Transfer went ok\n");
#   } else { 
#       print("Transfer didn't work");
#   }

#   post_blog_report($blog_id);
#}


#sub post_blog_report {
#   my ($biblio_id, $biblio_title, $review) = @_;
#   my $posted_review = curl_setopt($curl, CURLOPT_POST, $biblio_id, $biblio_title, $review);

#   if $posted_review {
#       warn "Review susccessfully posted";
#   }
#   else {
#       warn "Review not successfully posted";
#   }
#}

#sub get {
#   my $c = shift->openapi->valid_input or return;
#
#   my $city = Koha::Cities->find( $c->validation->param('cityid') );
#   unless ($city) {
#       return $c->render( status  => 404,
#                          openapi => { error => "City not found" } );
#   }
#
#   return $c->render( status => 200, openapi => $city );
#}

#sub add {
#   my $c = shift->openapi->valid_input or return;

#   my $city = Koha::City->new( $c->validation->param('body') );

#   return try {
#       $city->store;
#       return $c->render( status => 200, openapi => $city );
#   }
#   catch {
#       if ( $_->isa('DBIx::Class::Exception') ) {
#           return $c->render( status  => 500,
#                              openapi => { error => $_->{msg} } );
#       }
#       else {
#           return $c->render( status => 500,
#               openapi => { error => "Something went wrong, check the logs."} );
#       }
#   };
#}

#sub update {
#   my $c = shift->openapi->valid_input or return;
#
#   my $city;
#
#   return try {
#       $city = Koha::Cities->find( $c->validation->param('cityid') );
#       my $params = $c->req->json;
#       $city->set( $params );
#       $city->store();
#       return $c->render( status => 200, openapi => $city );
#   }
#   catch {
#       if ( not defined $city ) {
#           return $c->render( status  => 404,
#                              openapi => { error => "Object not found" } );
#       }
#       elsif ( $_->isa('Koha::Exceptions::Object') ) {
#           return $c->render( status  => 500,
#                              openapi => { error => $_->message } );
#       }
#       else {
#           return $c->render( status => 500,
#               openapi => { error => "Something went wrong, check the logs."} );
#       }
#   };

#}

#sub delete {
#   my $c = shift->openapi->valid_input or return;

#   my $city;

#   return try {
#       $city = Koha::Cities->find( $c->validation->param('cityid') );
#       $city->delete;
#       return $c->render( status => 200, openapi => "" );
#   }
#   catch {
#       if ( not defined $city ) {
#           return $c->render( status  => 404,
#                              openapi => { error => "Object not found" } );
#       }
#       elsif ( $_->isa('DBIx::Class::Exception') ) {
#           return $c->render( status  => 500,
#                              openapi => { error => $_->{msg} } );
#       }
#       else {
#           return $c->render( status => 500,
#               openapi => { error => "Something went wrong, check the logs."} );
#       }
#   };

#}

1;
